var gulp = require('gulp'), // Сообственно Gulp JS
    csso = require('gulp-csso'), // Минификация CSS
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    purge = require('gulp-css-purge'),
    minify = require('gulp-minify-css'),
    pump = require('pump'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer');


// Собираем JS
gulp.task('js', function() {
    gulp.src(['./libs/**/*.js', '!./libs/jquery.min.js', '!./libs/popper.min.js'])
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'))
});

/*gulp.task('compress', function (cb) {
    pump([
            gulp.src('./js/!*.js'),
            uglify(),
            gulp.dest('./js')
        ],
        cb
    );
});*/

gulp.task('scss', function () {
    gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(concat('main.min.css'))
        .pipe(autoprefixer({
            browsers: ['last 5 versions'],
            cascade: false
        }))
        .pipe(purge())
        .pipe(minify())
        .pipe(gulp.dest('./css/'));
});

gulp.task('build', function () {
    gulp.run('js');
    //gulp.run('compress');
    gulp.run('scss');
});

gulp.task('watch', function() {
    gulp.run('js');
    //gulp.run('compress');
    gulp.run('scss');
    gulp.watch('./libs/**/*.js', function() {
        gulp.run('js');
    });
    gulp.watch('./scss/**/*.scss', function() {
        gulp.run('scss');
    });
});